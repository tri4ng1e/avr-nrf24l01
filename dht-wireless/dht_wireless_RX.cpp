/*
	This is RX module of dht-wireless

	All myLibs you can find at https://bitbucket.org/Sky-WaLkeR/avr-mylibs
	You can watch video at http://youtu.be/2UIry5OYfpw

	Connections:
	PB0 - LED
	nRF - as described in myLibs/nrf24.h

	Payload description:
	data[0] - integer part of temperature
	data[1] - decimal part of temperature
	data[2] - integer part of humidity
	data[3] - decimal part of humidity
	data[4] - CHECK value - 1=OK, 0=corrupt data, 2=device not found
	data[5] - sign of temperature (+=0, -=1)

*/

#include <avr/io.h>
#include <util/delay.h>

#include <myLibs/libSPI.h>
#include <myLibs/types.h>
#include <myLibs/nrf24.h>
#include <myLibs/libDebug.h>

uchar addr[5]={0xe0, 0xe0, 0xe0, 0xe0, 0xe0}; // receive address
uchar data[16]; schar tmp;

int main(){
	DDRB=0x01; // for LED

	spi_init();
	mirf_init();
	debug_init();
	_delay_ms(50);

	mirf_set_RADDR(addr); // set receive address
	mirf_set_ch(38); // channel - any digit from 1 to 125

    while (1){
    	while (!mirf_data_ready());
    	mirf_get_data(data);
    	tmp=data[0]; if (data[5]==1) tmp*=(-1);

    	debug("CHECK="); debug((int)data[4]); debug("\tT=");
    	if (data[5]!=0) debug("-");
    	debug((int)tmp); debug('.'); debug((int)data[1]); debug("\tH=");
    	debug((int)data[2]); debug('.'); debug((int)data[3]); debug("\n");
    	PORTB|=1;
    	_delay_ms(100);
    	PORTB&=~1;
    }

	return 0;
}

