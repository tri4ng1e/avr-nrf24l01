/*
	This is TX module of dht-wireless

	All myLibs you can find at https://bitbucket.org/Sky-WaLkeR/avr-mylibs
	You can watch video at http://youtu.be/2UIry5OYfpw

	Connections:
	PB6 - LED
	nRF - as described in myLibs/nrf24.h
	 DHT:
	 1, 4 - VCC and GND
	 2 - PB0 and 10k resistor to VCC (pull-up)

	Payload description:
	data[0] - integer part of temperature
	data[1] - decimal part of temperature
	data[2] - integer part of humidity
	data[3] - decimal part of humidity
	data[4] - CHECK value - 1=OK, 0=corrupt data, 2=device not found
	data[5] - sign of temperature (+=0, -=1)

*/

#include <avr/io.h>
#include <util/delay.h>

#include <myLibs/libSPI.h>
#include <myLibs/types.h>
#include <myLibs/nrf24.h>
#include <myLibs/DHT22.h>

uchar addr[5]={0xe0, 0xe0, 0xe0, 0xe0, 0xe0}; // transmit address
uchar data[16], i;
int temp, hum; char sign;

int main(){
	DDRB=1<<6; // LED

	data[0]=0;

	spi_init();
	mirf_init();
	_delay_ms(50);

	mirf_set_TADDR(addr);

	mirf_config_register(RF_SETUP, 0b00100110);
	mirf_set_ch(38); // channel, any digit from 1 to 125. TX and RX channels MUST be same!
 	mirf_set_retr(5, _250uS); // if transmit fails, try to tetransmit 5 times with 250us delay

    while (1){
    	PORTB=1<<6;
    	data[4]=dht_read(&temp, &hum);
    	PORTB&=~(1<<6);

    	data[5]=(temp&(1<<15))>>8; temp&=~(1<<15);

    	data[0]=temp/10;
    	data[1]=temp%10;
    	data[2]=hum/10;
    	data[3]=hum%10;

        mirf_send(data,16);
        _delay_ms(1000);
    }

	return 0;
}

